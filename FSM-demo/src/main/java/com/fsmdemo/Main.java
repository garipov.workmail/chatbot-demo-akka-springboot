package com.fsmdemo;

import akka.actor.typed.ActorSystem;
import java.util.Scanner;

public class Main {


    public static void main(String[] args) {

        ActorSystem<Question.Command> mySystem = ActorSystem.create(Question.create(), "MySystem");

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter some word...");

        while (true) {
            String question = scanner.nextLine();
            mySystem.tell(new Question.AskQuestion(question));
            mySystem.log();
            if (question.equals("bye")) mySystem.terminate();
            if (question.equals("bye")) break;
        }

    }
}
