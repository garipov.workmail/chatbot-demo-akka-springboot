package com.fsmdemo;

import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

public class Question extends AbstractBehavior<Question.Command> {

    interface Command {
    }

    public static class AskQuestion implements Command {

        public final String newQuestion;

        public AskQuestion(String newQuestion) {
            this.newQuestion = newQuestion;
        }

    }

    public static Behavior<Command> create() {
        return Behaviors.setup(context -> new Question(context));
    }

    private String question = "";

    private Question(ActorContext<Command> context) {
        super(context);
    }

    @Override
    public Receive<Command> createReceive() {
        return newReceiveBuilder()
                .onMessage(AskQuestion.class, this::answerOnQuestion)
                .build();
    }

    private Behavior<Command> answerOnQuestion(AskQuestion command) {
        question = command.newQuestion;

        HttpClient httpClient = HttpClients.createDefault();
        HttpGet httpGet= new HttpGet("http://localhost:8080/ask?question=" + question);
        try {
            HttpResponse httpResponse = httpClient.execute(httpGet);
            String body = EntityUtils.toString(httpResponse.getEntity());
            System.out.println(body);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return this;
    }

}
