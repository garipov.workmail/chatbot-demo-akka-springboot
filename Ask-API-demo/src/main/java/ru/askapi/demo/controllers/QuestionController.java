package ru.askapi.demo.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class QuestionController {

    @GetMapping("/ask")
    public String getAnswer(Model model, @RequestParam("question") String question) {
        model.addAttribute("answer", question);
        return "answer";

    }
}
